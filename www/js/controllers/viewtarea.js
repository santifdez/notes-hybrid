function viewTareaCtrl ($scope,$rootScope, $routeParams) {

	$rootScope.headerColor = "#9C9C9C";
	$rootScope.headerTitle ="";
	$rootScope.optionsViewHeader = true;
	$scope.currentTask =null;
	$scope.isNewTask = true;
	$scope.activeColor  = "White";
	$scope.taskId = null;
	$scope.showRemider =false;
	$scope.showLocation = false;
	$rootScope.deleteAllItemsOption =false;
	$rootScope.addNewImage = true;
	
	//Reminder
	$scope.currentRemainderDay = "Hoy";
	$scope.currentRemainderPartDay = "por la mañana";
	$rootScope.deleteIcon =false;
	$rootScope.archiveIcon =false;
	$scope.containImage =false;
	$scope.nextWeekDay = calculateDayNextWeek();
 	
	if ($routeParams.tareaId != "null"){
		$scope.isNewTask =false;
		$rootScope.deleteIcon =true;
		$rootScope.archiveIcon =true;	
		$scope.taskId = $routeParams.tareaId;
		//Load Note or Task
		Tarea.read($routeParams.tareaId).then(function (tarea) {
			if (tarea.archive){
				$rootScope.img_archive = $scope.img_archive_out_small;
			}else{
				$rootScope.img_archive = $scope.img_archive_in_small;
			}
			if (tarea.trash){
				$rootScope.img_trash = $scope.img_close_trash_icon_small;
			}else{
				$rootScope.img_trash = $scope.img_open_trash_icon_small;
			}
			if (tarea.imagenTask){
				$scope.containImage =true;
				$rootScope.addNewImage = false;
			}else{
				$rootScope.addNewImage = true;
			}

			$scope.currentTask = tarea;
			$scope.activeColor =tarea.color;
		   //Force to reload variables
		   if($scope.currentTask.type==1){
		   		$rootScope.deleteIcon =true;
		   		$rootScope.archiveIcon =true;	
		   		$scope.showRemider = true;	
		   		$scope.currentRemainderDay = calculateMonth($scope.currentTask.date_reminder.getMonth())  + " "+ $scope.currentTask.date_reminder.getDate();
		   		$scope.currentRemainderPartDay = parseNumber($scope.currentTask.date_reminder.getHours()) +" "+ parseNumber($scope.currentTask.date_reminder.getMinutes());
		   }
		   	$scope.$apply();
		});
	}
	
	$scope.selectedColor = function(color){
		$scope.activeColor  = color;
		$('#colorPalette').modal('hide');
		if (!$scope.isNewTask){
			Tarea.read($scope.taskId).then(function (tarea) {
				tarea.date_updated = new Date().getTime();
				tarea.color = $scope.activeColor;
				return tarea.save(); 
			})  
		}
	}

	$scope.updateTitle = function(){
		if ($scope.isNewTask){
			$rootScope.deleteIcon =true;
			$rootScope.archiveIcon =true;	
			$scope.isNewTask =false;
			$scope.currentTask.trash =false;
			$scope.currentTask.title = $scope.currentTask.title;
			$scope.currentTask.type =0;
			$scope.currentTask.date_created = new Date().getTime();
			$rootScope.img_trash = $scope.img_open_trash_icon_small;
			$scope.currentTask.color = $scope.activeColor;
				Tarea.save($scope.currentTask).then(function(tarea){
            		$scope.$apply(function(){
               		$scope.taskId = tarea.initData.Id;
            	})
            })
		}else{
			Tarea.read($scope.taskId).then(function (tarea) {
				tarea.date_updated = new Date().getTime();
			 	tarea.title = $scope.currentTask.title;
			    return tarea.save(); 
			})  
		}
	}

	$scope.updateDescription = function(){
		if ($scope.isNewTask){
			$rootScope.deleteIcon =true;
			$rootScope.archiveIcon =true;
			$scope.isNewTask = false;
			$scope.currentTask.trash =false;
			$scope.currentTask.description = $scope.currentTask.description;
			$scope.currentTask.type =0;
			$scope.currentTask.date_created = new Date().getTime();
			$rootScope.img_trash = $scope.img_open_trash_icon_small;
			$scope.currentTask.color = $scope.activeColor;
				Tarea.save($scope.currentTask).then(function(tarea){
            		$scope.$apply(function(){
               		$scope.taskId = tarea.initData.Id;
            	})
            })
		}else{
			Tarea.read($scope.taskId).then(function (tarea) {
				tarea.date_updated = new Date().getTime();
				tarea.description  = $scope.currentTask.description;
			    return tarea.save(); 
			})  
		}
	}

	$scope.changeReminderStatus = function() {
		$scope.showRemider = true;
		//create Today Morning by default
		var today = new Date();
		today.setHours("09");
		today.setMinutes("00");
		
		if ($scope.isNewTask){
			$rootScope.deleteIcon =true;
			$rootScope.archiveIcon =true;
			$scope.isNewTask = false;
			$scope.tareas.trash =false;
			$scope.tareas.type =1;
			$scope.tareas.title = "Recordatorio " + calculateMonth(today.getMonth()) +" "+ today.getDate()+", 09:00"; 
			$scope.tareas.date_reminder = today;
			$scope.tareas.date_created = new Date().getTime();
			$scope.tareas.color ="white";
				Tarea.save($scope.tareas).then(function(tarea){
            		$scope.$apply(function(){
               		$scope.taskId = tarea.initData.Id;
            	})
            })
		}else{
			Tarea.read($scope.taskId).then(function (tarea) {
				tarea.date_updated = new Date().getTime();
				tarea.type =1;
				tarea.date_reminder = today;
			    return tarea.save(); 
			})  
		}
	

	}
	$scope.changeLocationStatus = function() {
		//$scope.showLocation = true;
	}
	$scope.closeReminder = function (){
		$scope.showRemider = false;	
		$scope.currentRemainderDay = "Hoy";
		$scope.currentRemainderPartDay = "por la mañana";
		Tarea.read($scope.taskId).then(function (tarea) {
			tarea.date_updated = new Date().getTime();
			tarea.type =0;
			tarea.date_reminder = null;
			cancelNotification($scope.taskId);
			return tarea.save(); 
		}) 

	}
	$scope.changeDate = function (dateSelected,addDays){
		if (dateSelected =="nextWeek"){
			$scope.currentRemainderDay = "Próximo " +calculateDayNextWeek();
		}else{
			$scope.currentRemainderDay = dateSelected;	
		}
		Tarea.read($scope.taskId).then(function (tarea) {
			tarea.date_updated = new Date().getTime();
			var date = new Date();
			var minutes = tarea.date_reminder.getMinutes();
			var hours = tarea.date_reminder.getHours();
			date.setMinutes(minutes);
			date.setHours(hours);
			tarea.date_reminder = date.setDate(date.getDate() + addDays);
			if(tarea.title){
				if(tarea.title.indexOf("Recordatorio") == 0  && !tarea.description){
					tarea.title = "Recordatorio " + calculateMonth(tarea.date_reminder.getMonth()) +" "+ tarea.date_reminder.getDate()+", "+parseNumber(tarea.date_reminder.getHours())+":"+parseNumber(tarea.date_reminder.getMinutes());
					if($scope.currentTask){
						$scope.currentTask.title = tarea.title;	
					}
				}
			}
			if (!tarea.trash){
				createNotification($scope.taskId,$scope.currentTask.title,date);
			}
			$scope.$apply();
			return tarea.save(); 
		})  
	}

	$scope.openYearCalendar = function(){

		var options = {
		  date: new Date(),
		  mode: 'date'
		};

		datePicker.show(options, function(date){
			 Tarea.read($scope.taskId).then(function (tarea) {
				tarea.date_updated = new Date().getTime();
				var minutes = tarea.date_reminder.getMinutes();
				var hours = tarea.date_reminder.getHours();
				date.setMinutes(minutes);
				date.setHours(hours);
				tarea.date_reminder = date.setDate(date.getDate());
				$scope.currentRemainderDay = calculateMonth(tarea.date_reminder.getMonth()) +" "+ tarea.date_reminder.getDate();
				if(tarea.title){
					if(tarea.title.indexOf("Recordatorio") == 0  && !tarea.description){
						tarea.title = "Recordatorio " + calculateMonth(tarea.date_reminder.getMonth()) +" "+ tarea.date_reminder.getDate()+", "+parseNumber(tarea.date_reminder.getHours())+":"+parseNumber(tarea.date_reminder.getMinutes());
						if($scope.currentTask){
							$scope.currentTask.title = tarea.title;	
						}
					}
				}
				if (!tarea.trash){
					createNotification($scope.taskId,$scope.currentTask.title,date);
				}
				
				$scope.$apply();
				return tarea.save(); 
			})  
		});
	}


	
	$scope.openDayCalendar = function(date){
		var options = {
		  date: new Date(),
		  mode: 'time'
		};
		datePicker.show(options, function(date){
			Tarea.read($scope.taskId).then(function (tarea) {
				tarea.date_updated = new Date().getTime();
				var newDate = tarea.date_reminder;
				newDate.setMinutes(date.getMinutes());
				newDate.setHours(date.getHours());
				tarea.date_reminder = newDate;
				$scope.currentRemainderPartDay = parseNumber(tarea.date_reminder.getHours())+":"+parseNumber(tarea.date_reminder.getMinutes());
				if(tarea.title){
					if(tarea.title.indexOf("Recordatorio") == 0  && !tarea.description){
						tarea.title = "Recordatorio " + calculateMonth(tarea.date_reminder.getMonth()) +" "+ tarea.date_reminder.getDate()+", "+parseNumber(tarea.date_reminder.getHours())+":"+parseNumber(tarea.date_reminder.getMinutes());
						if($scope.currentTask){
							$scope.currentTask.title = tarea.title;	
						}
					}
				}
				if (!tarea.trash){
					createNotification($scope.taskId,tarea.title,newDate);
				}
				
				$scope.$apply();
				return tarea.save(); 
			})  

		});
	}

	$scope.changeDay = function(partofday, newhour){
		$scope.currentRemainderPartDay = partofday;
		Tarea.read($scope.taskId).then(function (tarea) {
			tarea.date_updated = new Date().getTime();
			var date = tarea.date_reminder;
			date.setHours(newhour);
			date.setMinutes("00");
			tarea.date_reminder = date;
			if(tarea.tile){
				if(tarea.title.indexOf("Recordatorio") == 0  && !tarea.description){
					tarea.title = "Recordatorio " + calculateMonth(date.getMonth()) +" "+ date.getDate()+", "+parseNumber(date.getHours())+":"+parseNumber(date.getMinutes());
					if($scope.currentTask){
						$scope.currentTask.title = tarea.title;	
					}
				}
			}
			if (!tarea.trash){
				createNotification($scope.taskId,$scope.currentTask.title,date);
			}
			
			$scope.$apply();	
			return tarea.save(); 
		}) 
	}
	$rootScope.deleteTask = function(){
		if($rootScope.img_trash == $scope.img_close_trash_icon_small){
			$rootScope.img_trash = $scope.img_open_trash_icon_small;
		}else{
			$rootScope.img_trash = $scope.img_close_trash_icon_small;
		}

		Tarea.read($scope.taskId).then(function (tarea) {
			if (tarea.trash){
				tarea.trash = false;
				if(tarea.date_reminder){
					createNotification($scope.taskId,tarea.title,tarea.date_reminder);
				}
				
			}else{
				tarea.trash = true;	
				cancelNotification($scope.taskId);
			}
			tarea.date_updated = new Date().getTime();
			return tarea.save();
		}).then(function () {
    		//window.location = "#/"; 
		}); 

	}
	$rootScope.archiveTask = function(){
		if($rootScope.img_archive == $scope.img_archive_in_small){
			$rootScope.img_archive = $scope.img_archive_out_small;
		}else{
			$rootScope.img_archive = $scope.img_archive_in_small;
		}
	
		Tarea.read($scope.taskId).then(function (tarea) {
			if (tarea.archive){
				tarea.archive = false;	
			}else{
				tarea.archive = true;	
			}
			tarea.date_updated = new Date().getTime();
			return tarea.save();
		})
	}
	$scope.deleteImage = function(){
		//delete the just the image or delete the task if datereminder , title and description is empty
		$('#deleteImageSelected').modal('hide');
		Tarea.read($scope.taskId).then(function (tarea) {
			if (tarea.title || tarea.description || tarea.date_reminder){
				tarea.imagenTask = null;
				tarea.date_updated = new Date().getTime();
				$rootScope.addNewImage = true;
				$scope.$apply();
				return tarea.save();
			}else{
				$rootScope.deleteIcon =false;
				$rootScope.archiveIcon =false;
				$scope.isNewTask = true;
				$rootScope.addNewImage = true;
				tarea.remove();
				$scope.$apply();
			}
		})
		$scope.containImage =false;

	}

	$rootScope.addImageTask =function (){
		navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
	    destinationType: Camera.DestinationType.DATA_URL});

		function onSuccess(imageData) {
			
			if ($scope.isNewTask){
				$rootScope.deleteIcon =true;
				$rootScope.archiveIcon =true;
				$scope.isNewTask = false;
				$rootScope.addNewImage = false;
				$scope.containImage =true;

				$scope.tareas.type =0;
				$scope.tareas.trash =false;
				$scope.tareas.imagenTask = "data:image/jpeg;base64," + imageData;
				$scope.tareas.date_created = new Date().getTime();
				$scope.tareas.color = $scope.activeColor;
					Tarea.save($scope.tareas).then(function(tarea){
	            		$scope.$apply(function(){
	               		$scope.taskId = tarea.initData.Id;
	               		$scope.currentTask = tarea;

	            	})
	            })
			}else{
				Tarea.read($scope.taskId).then(function (tarea) {
					tarea.date_updated = new Date().getTime();
					tarea.imagenTask =  "data:image/jpeg;base64," + imageData;
				 	$scope.currentTask.imagenTask = "data:image/jpeg;base64," + imageData;
				 	$rootScope.addNewImage = false;
				 	$scope.containImage =true;
				 	$scope.$apply();			 
			   		return tarea.save(); 
				})  
			}
		}
		function onFail(message) {
		    //alert('Failed because: ' + message);
		}

	}
	/*$scope.testReminders = function(){
			window.plugin.notification.local.isScheduled(1, function (isScheduled) {
    		 alert('Notification with ID 1 is scheduled: ' + isScheduled);
		});
	}*/

} 
function test(){

	alert("test");
}

function calculateDayNextWeek(){
		var day;
    	switch (new Date().getDay()) {
		    case 0:
		        day = "domingo";
		        break;
		    case 1:
		        day = "lunes";
		        break;
		    case 2:
		        day = "martes";
		        break;
		    case 3:
		        day = "miércoles";
		        break;
		    case 4:
		        day = "jueves";
		        break;
		    case 5:
		        day = "viernes";
		        break;
		    case  6:
		        day = "sábado";
		        break;
  		}
  	return day;
}

function calculateMonth(monthNumber){
	var month = new Array();
	
	month[0] = "Enero";
	month[1] = "Febrero";
	month[2] = "Marzo";
	month[3] = "Abrill";
	month[4] = "Mayo";
	month[5] = "Junio";
	month[6] = "Julio";
	month[7] = "Agosto";
	month[8] = "Septiembre";
	month[9] = "Octubre";
	month[10] = "Noviembre";
	month[11] = "Diciembre";
	
	var monthString = month[monthNumber];
	return monthString;
}

function parseNumber(number){
	if (number<10){
		number = "0"+number;
	}
	return number;
}

function createNotification(idTask,title,taskDate){
	window.plugin.notification.local.add({
	    id:     idTask,
	    title:   'Recordatorio',
	    message: title,
	    date:    taskDate,
	    autoCancel:  true
	 	//sound: 'android.resource://' + package_name + '/raw/beep'
	   //sound: '/www/sound/notification.mp3'
	});

	
     window.plugin.notification.local.onclick = function(id, state, json){
        window.location = "#/viewtarea/"+id;
    };
   
}

function cancelNotification(idTask){
	window.plugin.notification.local.cancel(idTask, function () {});
}


