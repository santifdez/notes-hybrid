function tareasCtrl ($scope,$rootScope,$routeParams) {

	/*Load Screen according with filter*/
	$scope.filterToApply = $routeParams.filter;
	

	//Dirty code for exception
	$scope.exceptionTrashDelete =false;
	$rootScope.optionsViewHeader = false;
	$rootScope.displayAddNewNote= true;
	$rootScope.displayAddNewNoteQuickInput= true;

	$rootScope.deleteAllItemsOption =false;

	/*Definition Empty Results*/
	if($scope.filterToApply == $scope.filter_reminders){
		$rootScope.headerTitle ="Recordatorios";
		$scope.TitleEmptyItems ="No hay notas en recordatorios";
		$scope.SrcEmptyItems = $scope.img_reminder_icon_big;
		$rootScope.headerColor="#81796E";

	}else if($scope.filterToApply == $scope.filter_allArhives){	
		$rootScope.headerTitle ="Archivados";
		$scope.TitleEmptyItems ="No hay notas en archivadas";
		$scope.SrcEmptyItems = $scope.img_archive_icon_big;
		$rootScope.headerColor="#81796E";
		
	}else if($scope.filterToApply == $scope.filter_allDeletes){
		$rootScope.headerTitle ="Papelera";
		$scope.TitleEmptyItems ="No hay notas borradas";
		$scope.SrcEmptyItems = $scope.img_delete_icon_big;
		$rootScope.deleteAllItemsOption =true;
		$rootScope.headerColor="#42403C";	
	}else{
		$rootScope.headerTitle ="Notas";
		$scope.TitleEmptyItems ="No hay notas";
		$scope.SrcEmptyItems = $scope.img_notes_icon_big;
		$rootScope.headerColor = "#FF9B1B";
	}

	Tarea.readAll().then(function(tareas){
		$scope.$apply(function(){
			$scope.tareas = tareas;
		 	$scope.filterTask();

		});
	});

	$scope.createQuickNote =function (){
		if ($scope.newTarea.description) {
			$scope.newTarea.trash = false;
			$scope.newTarea.type =0;
			$scope.newTarea.date_created = new Date().getTime();
			$scope.newTarea.color ="white";
			$scope.newTarea.archive = false;
			 Tarea.save($scope.newTarea).then(function(tarea){
            	$scope.$apply(function(){
            		$scope.newTarea =null;
            		$scope.tareas.push(tarea);
            	})
            })
			$scope.newTarea.description= '';
		}		
	}
	
	$scope.createnewTask = function(){
		if ($scope.quickTask.$valid) {
			$scope.newTarea.trash =false;
			$scope.newTarea.type =0;
			$scope.newTarea.date_created = new Date().getTime();
			$scope.newTarea.color ="white";
			$scope.newTarea.archive = false;
			 Tarea.save($scope.newTarea).then(function(tarea){
            	$scope.$apply(function(){
            		var idNewTarea = tarea.initData.Id;
               		$scope.newTarea =null;
            		$scope.tareas.push(tarea);
            		window.location = "#/viewtarea/"+idNewTarea;
            	})
            })
			$scope.newTarea.description= '';
		}else{
			window.location = "#/viewtarea/"+null;
		}
	}
	$scope.displayTask =function(taskId){
		window.location = "#/viewtarea/"+taskId;
	}

	$scope.openMicrophone = function() {


	 	var maxMatches = 1;
        var promptString = "Se esta grabando"; // optional
        var language = "es-ES";                     // optional
        window.plugins.speechrecognizer.startRecognize(function(result){
			saveSpeechrecognizer(result, $scope);

        }, function(errorMessage){
                    console.log("Error message: " + errorMessage);
        }, maxMatches, promptString, language);
	}

	

	/*Apply Filter*/
	$scope.filterTask = function (){

		var hideDisplayNotes = false;
		if($scope.filterToApply == $scope.filter_reminders){
			$scope.filterExpr = { type: '1', trash: false  };
			$rootScope.displayAddNewNoteQuickInput= false;
		}else if($scope.filterToApply == $scope.filter_allArhives){	
			$scope.filterExpr = { trash: false ,archive: true};
			hideDisplayNotes =true;
		}else if($scope.filterToApply == $scope.filter_allDeletes){
			$scope.filterExpr = { trash: true };
			hideDisplayNotes =true;
		}else{
			$scope.filterExpr = { trash: false, archive: false};
			$scope.filterToApply =$scope.filter_allTasks;
		}
		if (hideDisplayNotes){
			$rootScope.displayAddNewNote= false;
			$rootScope.displayAddNewNoteQuickInput= false;
		}
	}

	$scope.deleteAllItemTrash = function(){
		
		Tarea.readAll().then(function(tareas){
		    tareas.forEach(function(tarea) {
		    	if (tarea.trash){
		    		tarea.remove();
		    	}
		    });
 		 });
		$scope.tareas = null;
		$scope.exceptionTrashDelete =true;
		$('#deleteAllItemsModal').modal('hide');

	}

	$scope.openCamera =function(){
		navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
	    destinationType: Camera.DestinationType.DATA_URL});

		function onSuccess(imageData) {
			
			if ($scope.quickTask.$valid) {
				$scope.newTarea.trash = false;
				$scope.tareas.title = null;;
				$scope.newTarea.type =0;
				$scope.newTarea.date_created = new Date().getTime();
				$scope.newTarea.color ="white";
				$scope.newTarea.imagenTask = "data:image/jpeg;base64," + imageData;
				$scope.newTarea.archive = false;
				 Tarea.save($scope.newTarea).then(function(tarea){
	            	$scope.$apply(function(){
	            		$scope.newTarea =null;
	            		$scope.tareas.push(tarea);
	            	})
	            })
				
				$scope.newTarea.description= '';
				$scope.$apply();
			
			}else{
				$scope.newTarea = $scope.tareas;
				$scope.newTarea.description ="";
				$scope.newTarea.title ="";
				$scope.newTarea.trash = false;
				$scope.newTarea.type =0;
				$scope.newTarea.date_created = new Date().getTime();
				$scope.newTarea.color ="white";
				$scope.newTarea.imagenTask = "data:image/jpeg;base64," + imageData;
				$scope.newTarea.archive = false;
				Tarea.save($scope.newTarea).then(function(tarea){
	            	$scope.$apply(function(){
	            		$scope.tareas.push(tarea);
	            	})
	            })
				$scope.$apply();
			
			}
		}

		function onFail(message) {
		    //alert('Failed because: ' + message);
		}
	}
} 

function saveSpeechrecognizer(result,$scope){

	$scope.tareas.title ="Recordatorio De Voz";
	$scope.tareas.trash =false;
	result = JSON.stringify(result);
	result = result.replace(/['"]+/g, '').replace(/\]/g, "").replace(/\[/g, "");
	$scope.tareas.description =result;
	$scope.tareas.imagenTask = null;
	$scope.tareas.type =0;
	$scope.tareas.date_created = new Date().getTime();
	$scope.tareas.color ="white";
	$scope.tareas.archive = false;
	Tarea.save($scope.tareas).then(function(tarea){
        $scope.$apply(function(){
            	$scope.newTarea =null;
            	$scope.tareas.push(tarea);
            })
        })

	$scope.newTarea.description= '';
}		
