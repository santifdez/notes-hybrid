var app = angular.module('tareasApp', ['ngRoute','ngTouch','ngResource']);
	
	app.config(['$routeProvider', function($routeProvider){
		$routeProvider.when('/:filter?',{
			templateUrl:'partials/tareas.html',
			controller:'tareasCtrl'});
		$routeProvider.when('/viewtarea/:tareaId',{
			templateUrl:'partials/viewtarea.html',
			controller:'viewTareaCtrl'});
	}]);
	
	app.directive('onEnter',function(){
		var linkFn = function(scope,element,attrs) {
		 element.bind("keypress", function(event) {
				  if(event.which === 13) {
					  scope.$apply(function() {
						scope.$eval(attrs.onEnter);
					  });
					  event.preventDefault();
				  }
			  });
		};
		
		return {
			link:linkFn
		};
	});
	app.controller("AppController", function($scope,$http,$location,$rootScope){
		$scope.showmenu=false;
		$scope.toggleMenu = function(){
			$scope.showmenu=($scope.showmenu) ? false : true;
			if($scope.showmenu){
				$rootScope.optionsViewHeader = false;
				$rootScope.deleteAllItemsOption =false;
			}else{
				if($location.$$path.indexOf("viewtarea") > -1){
					$rootScope.optionsViewHeader = true;
				}
				if($location.$$path.indexOf("allDeletes") > -1){
					$rootScope.deleteAllItemsOption =true;
				}	
			}
		}
		$scope.toggleLeftMenu = function(url){
			$scope.showmenu=($scope.showmenu) ? false : true;
			window.location = "#/"+url;
		}

	$scope.init = function () {
		//Load Left Menu
		$http({method: 'GET', url: 'data/left_menu.json'}).
			success(function(data, status, headers, config) {
				$scope.items = data;
		});
		//Create Links for leftMenu
		$scope.filter_allTasks="allTasks";
		$scope.filter_reminders="allReminders";
		$scope.filter_allArhives="allArchives";
		$scope.filter_allDeletes="allDeletes";
		
		//Gloabal Values
		//$scope.imgPath ="../img/";
    $scope.imgPath ="img/";
		$scope.img_open_trash_icon_small = $scope.imgPath + "open_trash.png";
		$scope.img_close_trash_icon_small = $scope.imgPath + "close_trash.png";
		$scope.img_color_palette = $scope.imgPath + "color_palette.png";
		$scope.img_archive_in_small = $scope.imgPath + "archive_in_small.png";
		$scope.img_archive_out_small = $scope.imgPath + "archive_out_small.png";
		$scope.img_reminder_icon_big = $scope.imgPath + "reminder-icon-big.png";
		$scope.img_archive_icon_big = $scope.imgPath + "archive-icon-big.png";
		$scope.img_delete_icon_big = $scope.imgPath + "trash-icon-big.png";
		$scope.img_notes_icon_big = $scope.imgPath + "notes-icon-big.png";
    $scope.img_task_icon = $scope.imgPath + "task_ico.png";
    $scope.img_microphone_icon = $scope.imgPath + "microphone_ico.png";
    $scope.img_camera_icon = $scope.imgPath + "camera_ico.png";
    $scope.img_bell_icon = $scope.imgPath + "bell_icon.png";
    $scope.img_location_icon = $scope.imgPath + "location_icon.png";
    $scope.img_clock_icon = $scope.imgPath + "clock_icon.png";
    $scope.img_add_image = $scope.imgPath + "camera_ico_small.png";




		$rootScope.img_archive = $scope.img_archive_in_small;
		$rootScope.img_trash = $scope.img_open_trash_icon_small;

		//Creat tareas
		$scope.tareas = [];

		var Tarea = $data.define("Tarea",{
			title: String,
			description: String,
			//Type 0 = note
			//Type 1 = reminder
			type: Number,
			date_created: Date,
			date_updated: Date,
			voice_path:String,
			trash:Boolean,
      imagenTask:String,
			archive:Boolean,
			date_reminder:Date,
			color:String
		});
		//Define Circle colors
		$scope.circle_white = "white";
		$scope.circle_red = "rgba(240, 23, 23, 0.58)";
		$scope.circle_orange = "rgb(241, 176, 38)";
		$scope.circle_grass = "rgb(194, 201, 29)";
		$scope.circle_grey = "rgba(95, 93, 93, 0.43)";
		$scope.circle_blue = "rgba(0, 255, 245, 0.48)";
		$scope.circle_clearblue = "rgba(30, 168, 113, 0.37)";
		$scope.circle_green = "rgba(0, 255, 31, 0.86)";
	};
});

app.constant('msdElasticConfig', {
    append: ''
 })

app.directive('msdElastic', ['$timeout', '$window', 'msdElasticConfig', function($timeout, $window, config) {
    'use strict';

    return {
      require: 'ngModel',
      restrict: 'A, C',
      link: function(scope, element, attrs, ngModel) {

        // cache a reference to the DOM element
        var ta = element[0],
            $ta = element;

        // ensure the element is a textarea, and browser is capable
        if (ta.nodeName !== 'TEXTAREA' || !$window.getComputedStyle) {
          return;
        }

        // set these properties before measuring dimensions
        $ta.css({
          'overflow': 'hidden',
          'overflow-y': 'hidden',
          'word-wrap': 'break-word'
        });

        // force text reflow
        var text = ta.value;
        ta.value = '';
        ta.value = text;

        var appendText = attrs.msdElastic || config.append,
            append = appendText === '\\n' ? '\n' : appendText,
            $win = angular.element($window),
            mirrorStyle = 'position: absolute; top: -999px; right: auto; bottom: auto; left: 0 ;' +
                          'overflow: hidden; -webkit-box-sizing: content-box;' +
                          '-moz-box-sizing: content-box; box-sizing: content-box;' +
                          'min-height: 0 !important; height: 0 !important; padding: 0;' +
                          'word-wrap: break-word; border: 0;',
            $mirror = angular.element('<textarea tabindex="-1" ' +
                                      'style="' + mirrorStyle + '"/>').data('elastic', true),
            mirror = $mirror[0],
            taStyle = getComputedStyle(ta),
            resize = taStyle.getPropertyValue('resize'),
            borderBox = taStyle.getPropertyValue('box-sizing') === 'border-box' ||
                        taStyle.getPropertyValue('-moz-box-sizing') === 'border-box' ||
                        taStyle.getPropertyValue('-webkit-box-sizing') === 'border-box',
            boxOuter = !borderBox ? {width: 0, height: 0} : {
                          width:  parseInt(taStyle.getPropertyValue('border-right-width'), 10) +
                                  parseInt(taStyle.getPropertyValue('padding-right'), 10) +
                                  parseInt(taStyle.getPropertyValue('padding-left'), 10) +
                                  parseInt(taStyle.getPropertyValue('border-left-width'), 10),
                          height: parseInt(taStyle.getPropertyValue('border-top-width'), 10) +
                                  parseInt(taStyle.getPropertyValue('padding-top'), 10) +
                                  parseInt(taStyle.getPropertyValue('padding-bottom'), 10) +
                                  parseInt(taStyle.getPropertyValue('border-bottom-width'), 10)
                        },
            minHeightValue = parseInt(taStyle.getPropertyValue('min-height'), 10),
            heightValue = parseInt(taStyle.getPropertyValue('height'), 10),
            minHeight = Math.max(minHeightValue, heightValue) - boxOuter.height,
            maxHeight = parseInt(taStyle.getPropertyValue('max-height'), 10),
            mirrored,
            active,
            copyStyle = ['font-family',
                         'font-size',
                         'font-weight',
                         'font-style',
                         'letter-spacing',
                         'line-height',
                         'text-transform',
                         'word-spacing',
                         'text-indent'];

        // exit if elastic already applied (or is the mirror element)
        if ($ta.data('elastic')) {
          return;
        }

        // Opera returns max-height of -1 if not set
        maxHeight = maxHeight && maxHeight > 0 ? maxHeight : 9e4;

        // append mirror to the DOM
        if (mirror.parentNode !== document.body) {
          angular.element(document.body).append(mirror);
        }

        // set resize and apply elastic
        $ta.css({
          'resize': (resize === 'none' || resize === 'vertical') ? 'none' : 'horizontal'
        }).data('elastic', true);

        /*
         * methods
         */

        function initMirror() {
          mirrored = ta;
          // copy the essential styles from the textarea to the mirror
          taStyle = getComputedStyle(ta);
          angular.forEach(copyStyle, function(val) {
            mirrorStyle += val + ':' + taStyle.getPropertyValue(val) + ';';
          });
          mirror.setAttribute('style', mirrorStyle);
        }

        function adjust() {
          var taHeight,
              mirrorHeight,
              width,
              overflow;

          if (mirrored !== ta) {
            initMirror();
          }

          // active flag prevents actions in function from calling adjust again
          if (!active) {
            active = true;

            mirror.value = ta.value + append; // optional whitespace to improve animation
            mirror.style.overflowY = ta.style.overflowY;

            taHeight = ta.style.height === '' ? 'auto' : parseInt(ta.style.height, 10);

            // update mirror width in case the textarea width has changed
            width = parseInt(getComputedStyle(ta).getPropertyValue('width'), 10) - boxOuter.width;
            mirror.style.width = width + 'px';

            mirrorHeight = mirror.scrollHeight;

            if (mirrorHeight > maxHeight) {
              mirrorHeight = maxHeight;
              overflow = 'scroll';
            } else if (mirrorHeight < minHeight) {
              mirrorHeight = minHeight;
            }
            mirrorHeight += boxOuter.height;

            ta.style.overflowY = overflow || 'hidden';

            if (taHeight !== mirrorHeight) {
              ta.style.height = mirrorHeight + 'px';
              scope.$emit('elastic:resize', $ta);
            }

            // small delay to prevent an infinite loop
            $timeout(function() {
              active = false;
            }, 1);

          }
        }

        function forceAdjust() {
          active = false;
          adjust();
        }

        /*
         * initialise
         */

        // listen
        if ('onpropertychange' in ta && 'oninput' in ta) {
          // IE9
          ta['oninput'] = ta.onkeyup = adjust;
        } else {
          ta['oninput'] = adjust;
        }

        $win.bind('resize', forceAdjust);

        scope.$watch(function() {
          return ngModel.$modelValue;
        }, function(newValue) {
          forceAdjust();
        });

        scope.$on('elastic:adjust', function() {
          forceAdjust();
        });

        $timeout(adjust);

        /*
         * destroy
         */

        scope.$on('$destroy', function() {
          $mirror.remove();
          $win.unbind('resize', forceAdjust);
        });
      }
    };

}]);


